
# GIT LAB

## 作業 0

* 註冊一個 [bitbucket](https://bitbucket.org/) 帳號, 並至該[網址](http://notepad.cc/leoboru38)將你的帳號加入

* 設定 ssh public key for bitbucket ([教學網址](https://confluence.atlassian.com/display/BITBUCKET/Set+up+SSH+for+Git))

* 將repository複製到本地端

```
git clone git@bitbucket.org:ezecteam/git-lab.git
```

_Tips_ 記得閱讀每個指令執行後的訊息


## 作業 1

* 更新你的develop branch

```
git checkout develop && git pull
```

* checkout a feature branch from your current branch (develop)

```
git checkout -b <branch的名稱>
```

* 打開資料夾ass1中的index.html並加入你的名字, 存檔然後關閉 

* 將修改的檔案加入index並commit

```
git add index.html
git commit -m '你的commit訊息'
```

* 切換到你的develop branch

```
git checkout develop
```

* merge feature branch and push to remote develop branch

```
git merge <branch的名稱>
git push
```
   
   
_Tips_ 沒事就可以打一下git status 以免迷失了自己
   
   
## 作業 2
<<<<<<< HEAD
- checkout a feature branch named with your id and suffix with 'ass2' from master branch. (e.g. rocko-ass2)
- put your "first name" to the index.html in the folder ass2.
- commit the change
- switch to develop branch
- put your "whole name" to the file index.html in the folder ass2 and push the change to the remote develop.
- checkout the feature branch you just created.
- try to push to the develop branch.

Tips
=======

* branch from develop branch

```
git checkout -b <branch的名稱> develop
```

* 打開資料夾ass2中的index.html並加入你的名字, 存檔然後關閉

* 將修改的部分commit

```
git commit -a -m '你的commit訊息'
```

* 切換到你的 develop branch

```
git checkout develop
```

* 打開資料夾ass2中的index.html加入你的名字並在後面加上日期, 存檔關閉.

* 將修改的部分commit並push

```
git commit -a -m '你的commit訊息'
git push
```

* 將稍早commit的feature branch merge到你的develop, 此時你會遇到衝突, 解決衝突後將最後的結果push上去.

```
git merge <branch的名稱>
解決衝突
git push
```
   
_Tips_ git diff --check 可以找出多餘的空白字元
   
>>>>>>> aca8ead1677ab991b490157f5ecbff14897bf7e5
